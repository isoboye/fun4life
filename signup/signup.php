<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themeht.com/bootsland/html/login-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Jun 2020 11:11:49 GMT -->
<head>

<!-- meta tags -->
<meta charset="utf-8">
<meta name="keywords" content="bootstrap 4, premium, multipurpose, sass, scss, saas" />
<meta name="description" content="Bootstrap 4 Landing Page Template" />
<meta name="author" content="www.themeht.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Title -->
<title>Sign-up page</title>

<!-- Favicon Icon -->
<link rel="shortcut icon" href="img/Fun4life.png" />

<!-- inject css start -->

<link href="assets/css/theme-plugin.css" rel="stylesheet" />
<link href="assets/css/theme.min.css" rel="stylesheet" />

<!-- inject css end -->

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>

<body>

<!-- page wrapper start -->

<div class="page-wrapper">
  
<!-- preloader start -->

<div id="ht-preloader">
  <div class="loader clear-loader">
    <span></span>
    <p>Fun4life</p>
  </div>
</div>

<!-- preloader end -->




<!--hero section start-->

<section class="position-relative">
  <div id="particles-js"></div>
  <div class="container">
    <div class="row  text-center">
      <div class="col">
        <h1>Sign Up</h1>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb justify-content-center bg-transparent p-0 m-0">
            <li class="breadcrumb-item"><a class="text-dark" href="#">Home</a>
            </li>
            <li class="breadcrumb-item">Pages</li>
            <li class="breadcrumb-item">Account</li>
            <li class="breadcrumb-item active text-primary" aria-current="page">Sign Up</li>
          </ol>
        </nav>
      </div>
    </div>
    <!-- / .row -->
  </div>
  <!-- / .container -->
</section>

<!--hero section end--> 


<!--body content start-->

<div class="page-content">

<!--login start-->

<section>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-7 col-12">
        <img class="img-fluid" src="assets/images/login.png" alt="">
      </div>
      <div class="col-lg-5 col-12">
        <div>
          <h2 class="mb-3">Sign Up</h2>
          <form id="signup-form" method="post" action="http://themeht.com/bootsland/html/php/contact.php">
            <div class="messages" id="message"></div>
            <div class="form-group">
              <input id="form_fname" type="text" name="fname" class="form-control" placeholder="First name" required="required" data-error="Firstname is required.">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
              <input id="form_lname" type="text" name="lname" class="form-control" placeholder="Last name" required="required" data-error="Lastname is required.">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
              <select name="gender" id='gender' class='form-control' required="required" data-error="Gender is required.">
                <option selected  required="required"  > Gender
                  <div class="help-block with-errors"></div>

              <option value='Male'>Male</option>
              <div class="help-block with-errors"></div>

              <option value="Female">Female</option>
              <div class="help-block with-errors"></div>

              <option value='Other'>Other</option>
                <div class="help-block with-errors"></div>
            </select>
            </div>
            <div class="form-group">
              <input id="form_email" type="email" name="email" class="form-control" placeholder="Email" required="required" data-error="Email is required.">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
              <input id="form_mnumber" type="number" name="mnumber" class="form-control" placeholder="Mobile Number" required="required" data-error="Mobile Number is required.">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
              <input id="form_uname" type="text" name="uname" class="form-control" placeholder="User name" required="required" data-error="Username is required.">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
              <input id="form_password" type="password" name="password" class="form-control" placeholder="Password" required="required" data-error="password is required.">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
              <input id="form_cpassword" type="password" name="cpassword" class="form-control" placeholder="Confirm Password" required="required" data-error="confirm password is required.">
              <div class="help-block with-errors"></div>
            </div>

            <button type="submit" class="btn btn-primary btn-block" id="signupBtn"> Sign Up </button>

          </form>
          <div class="d-flex align-items-center text-center justify-content-center mt-4">
                  <span class="text-muted mr-1">Already have an account?</span>
                   <a href="signin.html">Sign In</a>
                </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--login end-->

</div>

<!--body content end--> 


<!--footer start-->

<footer class="py-11 bg-primary position-relative" data-bg-img="assets/images/bg/03.png">
  <div class="shape-1" style="height: 150px; overflow: hidden;">
    <svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;">
      <path d="M0.00,49.98 C150.00,150.00 271.49,-50.00 500.00,49.98 L500.00,0.00 L0.00,0.00 Z" style="stroke: none; fill: #fff;"></path>
    </svg>
  </div>
  <div class="container mt-11">
    <div class="row">
      <div class="col-12 col-lg-5 col-xl-4 mr-auto mb-6 mb-lg-0">
        <div class="subscribe-form bg-warning-soft p-5 rounded">
          <h5 class="mb-4 text-white">Newsletter</h5>
          <h6 class="text-light">Subscribe Our Newsletter</h6>
          <form id="mc-form" class="group">
            <input type="email" value="" name="EMAIL" class="email form-control" id="mc-email" placeholder="Email Address" required="" style="height: 60px;">
            <input class="btn btn-outline-light btn-block mt-3 mb-2" type="submit" name="subscribe" value="Subscribe">
          </form> <small class="text-light">Get started for 1 Month free trial No Purchace required.</small>
        </div>
      </div>
      <div class="col-12 col-lg-6 col-xl-7">
        <div class="row">
          <div class="col-12 col-sm-4 navbar-dark">
            <h5 class="mb-4 text-white">Pages</h5>
            <ul class="navbar-nav list-unstyled mb-0">
              <li class="mb-3 nav-item"><a class="nav-link" href="about-us-1.html">About</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="product-grid.html">Shop</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="faq.html">Faq</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="blog-card.html">Blogs</a>
              </li>
              <li class="nav-item"><a class="nav-link" href="contact-us.html">Contact Us</a>
              </li>
            </ul>
          </div>
          <div class="col-12 col-sm-4 mt-6 mt-sm-0 navbar-dark">
            <h5 class="mb-4 text-white">Service</h5>
            <ul class="navbar-nav list-unstyled mb-0">
              <li class="mb-3 nav-item"><a class="nav-link" href="#">Content Writing</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="#">Documentation</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="login.html">Account</a>
              </li>
              <li class="nav-item"><a class="nav-link" href="career.html">Careers</a>
              </li>
            </ul>
          </div>
          <div class="col-12 col-sm-4 mt-6 mt-sm-0 navbar-dark">
            <h5 class="mb-4 text-white">Legal</h5>
            <ul class="navbar-nav list-unstyled mb-0">
              <li class="mb-3 nav-item"><a class="nav-link" href="terms-and-conditions.html">Term Of Service</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="privacy-policy.html">Privacy Policy</a>
              </li>
              <li class="nav-item"><a class="nav-link" href="#">Support</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="row mt-5">
          <div class="col-12 col-sm-6">
            <a class="footer-logo text-white h2 mb-0" href="index.html">
              Boots<span class="font-weight-bold">Land.</span>
            </a>
          </div>
          <div class="col-12 col-sm-6 mt-6 mt-sm-0">
            <ul class="list-inline mb-0">
              <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-facebook"></i></a>
              </li>
              <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-dribbble"></i></a>
              </li>
              <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-instagram"></i></a>
              </li>
              <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-twitter"></i></a>
              </li>
              <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-linkedin"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row text-white text-center mt-8">
      <div class="col">
        <hr class="mb-8">Copyright 2019 Bootsland Theme by <u><a class="text-white" href="#">ThemeHt</a></u> | All Rights Reserved</div>
    </div>
  </div>
</footer>

<!--footer end-->

</div>

<!-- page wrapper end -->
 
<!--back-to-top start-->

<div class="scroll-top"><a class="smoothscroll" href="#top"><i class="las la-angle-up"></i></a></div>

<!--back-to-top end-->

<!-- inject js start -->

<script src="assets/js/theme-plugin.js"></script>
<script src="assets/js/theme-script.js"></script>

<!-- inject js end -->
<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.15.5/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/7.13.1/firebase-analytics.js"></script>

<script src="https://www.gstatic.com/firebasejs/7.13.1/firebase-auth.js"></script>

<script src="https://www.gstatic.com/firebasejs/7.13.1/firebase-firestore.js"></script>

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyCy20NngTtfNDewIdqlzsYYVZB65NA-Mi8",
    authDomain: "fun4life-e4d9b.firebaseapp.com",
    databaseURL: "https://fun4life-e4d9b.firebaseio.com",
    projectId: "fun4life-e4d9b",
    storageBucket: "fun4life-e4d9b.appspot.com",
    messagingSenderId: "850671214905",
    appId: "1:850671214905:web:2788313c891b83239888e8",
    measurementId: "G-H9YYY39KB5"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
//
const auth = firebase.auth();
const db = firebase.firestore();
</script>
<script src="./modules/public_module.js"
> </script>
</body>


<!-- Mirrored from themeht.com/bootsland/html/login-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Jun 2020 11:11:50 GMT -->
</html>
