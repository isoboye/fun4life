//variables
const sform =  document.getElementById('signup-form');
const submitRegBtn = document.getElementById('signupBtn');
const sloader = document.getElementById('signupLoader');
const message = document.getElementById('message');
var isvalid = false;
var isauthvalid = false;
var isvalidGender = false;
var d = new Date();
var y = d.getFullYear().toString();
var da = d.getDate().toString();
var m = d.getMonth().toString();
var mi = d.getMinutes().toString();
var h = d.getHours().toString();
var fdate = y + m + da + h + mi; 

sform.addEventListener('submit', (e)=>{
    submitRegBtn.innerHTML=`<div class="spinner spinner-border" id="signupLoader"></div>`;
   e.preventDefault();

   //form datas
   var fname = sform['form_fname'].value;
   var lname = sform['form_lname'].value;
   var gender  = sform['gender'].value;
   var email = sform['form_email'].value;
   var mnumber = sform['form_mnumber'].value;
   var uname = sform['form_uname'].value;
   var pwd = sform['form_password'].value;
   var pwd1 = sform['form_cpassword'].value;
   
   //validation
   //password
    if(pwd1 != pwd){

        isvalid = false;
       alert("Invalid password format!");
       submitRegBtn.innerHTML="Create Account";
       return;

    }else if(pwd < 5){

        isvalid = false;
        alert("Your password is too short!");
        return;

    }else{

        isvalid = true;
    }
    //Gender
    if(gender ==""){
        isvalidGender= false;
       alert ("Please Select a Gender!");
       submitRegBtn.innerHTML="Create Account";
       return;
    }else{
        isvalidGender = true;
    }

    //submit form

    if(isvalidGender == true && isauthvalid == true){
        auth.createUserWithEmailandPassword(email, pwd).then(cred=> {

                        var id = cred.user.uid;

            db.collection('users').doc(cred.user.uid).set({
                       Fname: fname,
                       Lname: lname,
                       Gender: gender,
                       Email: email,
                       Mnumber: mnumber,
                       Uname: uname,
                       RegisteredOn: fdate,
                       id:id         

            }).then(function(){
              
                 swal("success","Account created successfully", "success");

                 window.location ="../index.html";
                 sform.reset();

            }).catch(function (error){

           alert(error.message);
           submitRegBtn.innerHTML="Create Account";
           
            })
        }).catch(function (error){

            alert(error.message);
            submitRegBtn.innerHTML="Create Account";
        })
    }

})